// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCV1brq9O--owv_lENt2ijn0ytzuPea_98',
    authDomain: 'prueba-conocimiento.firebaseapp.com',
    databaseURL: 'https://prueba-conocimiento.firebaseio.com',
    projectId: 'prueba-conocimiento',
    storageBucket: 've4m3dYS2cKnFcZAa2RXHYjW8bnHn0lb0vr7gMyZ',
    messagingSenderId: '474226254698',
    appId: '1:474226254698:web:fa0914734efd1876f1fb13'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
