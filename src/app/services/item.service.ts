import {Injectable} from '@angular/core';
import {FirebaseDatabase} from '@angular/fire';
import {AngularFireDatabase} from '@angular/fire/database';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private modelo = 'items';

  constructor(private angularFireDatabase: AngularFireDatabase) {
  }

  listarTodos() {
    return this.angularFireDatabase.list(this.modelo).snapshotChanges().pipe(map(changes => changes as any));
  }

  agregar(data) {
    return this.angularFireDatabase.list(this.modelo).push(data);
  }

  actualizar(data) {
    return this.angularFireDatabase.list(this.modelo).update(data.key, {id: data.id, title: data.title});
  }

  eliminar(key) {
    return this.angularFireDatabase.list(this.modelo).remove(key);
  }
}
