import {Injectable} from '@angular/core';
import {Message} from 'primeng/api';
import {Subject} from 'rxjs';
import * as jQuery from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  msgs: Message = {severity: 'info', summary: 'Error', detail: 'Error al guardar / Intente nuevamente.'};
  msgAdd: Message = {severity: 'success', summary: 'Exito', detail: 'Registro agregado correctamente.'};
  msgUpdate: Message = {severity: 'success', summary: 'Exito', detail: 'Registro actualizado correctamente.'};
  msgError: Message = {severity: 'error', summary: 'Error', detail: 'Error al guardar / Intente nuevamente.'};
  subject = new Subject<Message>();
  getMessage$ = this.subject.asObservable();

  constructor() {
  }

  setMesage( type: number, msgCustom: Message = null, hide = true ) {
    switch ( type ) {
      case 1:
        this.msgs = this.msgAdd;
        break;
      case 2:
        this.msgs = this.msgUpdate;
        break;
      case 3:
        this.msgs = this.msgError;
        break;
      default:
        this.msgs = msgCustom;
        break;
    }
    jQuery( '#msgNotificacion' ).hide();
    this.subject.next( this.msgs );
    jQuery( '#msgNotificacion' ).slideDown( 400 );
    if ( hide ) {
      setTimeout( () => {
        jQuery( '#msgNotificacion' ).slideUp( 200 );
      }, 5000 );
    }
  }
}
