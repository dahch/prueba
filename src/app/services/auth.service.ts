import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private angularFireAuth: AngularFireAuth) {
  }

  estadoConexion() {
    return this.angularFireAuth.authState.pipe(map(res => res as any));
  }

  hacerLoginGoogle() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.angularFireAuth.auth.signInWithPopup(provider).then(res => {
        resolve(res);
      }).catch(error => {
        reject(error);
      });
    });
  }

  logOut() {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.auth.signOut().then(res => {
        resolve(res);
      }, error => reject(error));
    });
  }

  hacerRegistro(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
        .then(res => {
          resolve(res);
        }, error => reject(error));
    });
  }
}
