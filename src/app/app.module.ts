import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormSharedModule} from './shared/form-shared/form-shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {IngresoComponent} from './pages/ingreso/ingreso.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {HttpClientModule} from '@angular/common/http';
import {ListadoEndpointComponent} from './pages/listado-endpoint/listado-endpoint.component';
import {ListadoFirebaseComponent} from './pages/listado-firebase/listado-firebase.component';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import { AgregarItemComponent } from './pages/agregar-item/agregar-item.component';

@NgModule({
  declarations: [
    AppComponent,
    IngresoComponent,
    ListadoEndpointComponent,
    ListadoFirebaseComponent,
    AgregarItemComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormSharedModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
