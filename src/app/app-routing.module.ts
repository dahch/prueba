import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IngresoComponent} from './pages/ingreso/ingreso.component';
import {ListadoEndpointComponent} from './pages/listado-endpoint/listado-endpoint.component';
import {ListadoFirebaseComponent} from './pages/listado-firebase/listado-firebase.component';


const routes: Routes = [
  {path: '', redirectTo: '/ingreso', pathMatch: 'full'},
  {path: 'ingreso', component: IngresoComponent},
  {path: 'listado-ep', component: ListadoEndpointComponent},
  {path: 'listado-fb', component: ListadoFirebaseComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
