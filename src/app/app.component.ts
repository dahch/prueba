import {Component, OnInit} from '@angular/core';
import {MenuItem, Message} from 'primeng/api';
import {NavService} from './services/nav.service';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  msgs: Message[] = [];
  estadoConexion = false;
  items: MenuItem[];

  constructor(private navService: NavService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Endpoind',
        url: 'listado-ep',
        icon: 'pi pi-fw pi-globe',
      },
      {
        label: 'Firebase',
        url: 'listado-fb',
        icon: 'pi pi-fw pi-cloud',
      }
    ];
    this.authService.estadoConexion().subscribe(res => {
      this.estadoConexion = !!res;
      if (!this.estadoConexion) {
        this.router.navigate(['ingreso']);
      }
    });
    this.navService.getMessage$.subscribe(
      msgs => {
        this.msgs = [];
        this.msgs.push(msgs);
      });
  }

  cerrar() {
    this.authService.logOut();
  }

}
