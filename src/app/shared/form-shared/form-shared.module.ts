import {ModuleWithProviders, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {MessagesModule} from 'primeng/messages';
import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import {TableModule} from 'primeng/table';
import {MenubarModule} from 'primeng/menubar';

@NgModule( {
  declarations: [],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    ButtonModule,
    PanelModule,
    InputTextModule,
    MessagesModule,
    TableModule,
    MenubarModule
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    ButtonModule,
    PanelModule,
    InputTextModule,
    MessagesModule,
    TableModule,
    MenubarModule
  ],
  providers: []
} )

export class FormSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: FormSharedModule,
      providers: []
    };
  }
}
