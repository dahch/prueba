import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ItemService} from '../../services/item.service';
import {NavService} from '../../services/nav.service';

@Component({
  selector: 'app-agregar-item',
  templateUrl: './agregar-item.component.html',
  styleUrls: ['./agregar-item.component.scss']
})
export class AgregarItemComponent implements OnInit {

  @Input() item = null;
  @Output() salir: EventEmitter<boolean> = new EventEmitter<boolean>();
  formulario: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private itemService: ItemService,
              private navService: NavService) {
  }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      id: [this.item ? this.item.id : null, [Validators.required]],
      title: [this.item ? this.item.title : null, [Validators.required]],
    });
  }

  onSubmit() {
    if (this.formulario.valid) {
      if (this.item) {
        const data = this.formulario.value;
        data.key = this.item.key;
        this.itemService.actualizar(data);
        this.navService.setMesage(2, null, false);
      } else {
        this.itemService.agregar(this.formulario.value);
        this.navService.setMesage(1, null, false);
      }
      this.salir.emit(true);
    }
  }

}
