import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NavService} from '../../services/nav.service';

@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.scss']
})
export class IngresoComponent implements OnInit {

  formulario: FormGroup;

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private navService: NavService,
              private router: Router) {
  }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      email: [null, [Validators.required, Validators.maxLength(128), Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,3}$')]],
      password: [null, [Validators.required]]
    });
  }

  ingresarGoogle() {
    this.authService.hacerLoginGoogle().then(res => {
      this.avanzar();
    }, err => {
      this.navService.setMesage(0, {severity: 'error', summary: 'Error', detail: 'Intente nuevamente.'}, false);
    });
  }

  get f() {
    return this.formulario.controls;
  }

  onSubmit() {
    if (this.formulario.valid) {
      this.authService.hacerRegistro(this.formulario.value).then(res => {
        this.avanzar();
      }, err => {
        this.navService.setMesage(0, {severity: 'error', summary: 'Error', detail: 'Intente nuevamente.'}, false);
      });
    }
  }

  avanzar() {
    this.router.navigate(['listado-ep']);
  }

}
