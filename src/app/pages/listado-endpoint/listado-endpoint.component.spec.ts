import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoEndpointComponent } from './listado-endpoint.component';

describe('ListadoEndpointComponent', () => {
  let component: ListadoEndpointComponent;
  let fixture: ComponentFixture<ListadoEndpointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoEndpointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoEndpointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
