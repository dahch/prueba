import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-listado-endpoint',
  templateUrl: './listado-endpoint.component.html',
  styleUrls: ['./listado-endpoint.component.scss']
})
export class ListadoEndpointComponent implements OnInit {

  items: { id: number, title: string }[] = [];
  columnas: any[];
  carTabla = true;

  constructor(private http: HttpClient) {
    this.columnas = [
      {field: 'id', header: 'Id', sort: false},
      {field: 'title', header: 'Title', sort: false},
    ];
  }

  ngOnInit() {
    this.http.get('https://jsonplaceholder.typicode.com/todos').subscribe((res: { id: number, title: string }[]) => {
      this.carTabla = false;
      this.items = res;
      console.log(res);
    });
  }

}
