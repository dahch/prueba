import {Component, OnInit} from '@angular/core';
import {ItemService} from '../../services/item.service';
import {NavService} from '../../services/nav.service';

@Component({
  selector: 'app-listado-firebase',
  templateUrl: './listado-firebase.component.html',
  styleUrls: ['./listado-firebase.component.scss']
})
export class ListadoFirebaseComponent implements OnInit {

  items: { id: number, title: string }[] = [];
  columnas: any[];
  carTabla = true;
  mostrarFormulario = false;
  item: any = null;

  constructor(private itemService: ItemService, private navService: NavService) {
    this.columnas = [
      {field: 'id', header: 'Id', sort: false},
      {field: 'title', header: 'Title', sort: false},
      {field: 'det', header: 'Acciones', sort: false}
    ];
  }

  ngOnInit() {
    this.itemService.listarTodos().subscribe(datos => {
      this.items = datos.map(d => ({key: d.payload.key, ...d.payload.val()}));
      console.log(this.items);
      this.carTabla = false;
    });
  }

  agregar() {
    this.item = null;
    this.mostrarFormulario = true;
  }

  editar(item: any) {
    this.item = item;
    this.mostrarFormulario = true;
  }

  eliminar(item: any) {
    this.itemService.eliminar(item.key);
    this.navService.setMesage(0, {severity: 'success', summary: 'Exito', detail: 'Registro eliminado correctamente.'}, false);
  }

  cerrar(evento) {
    if (Boolean(evento)) {
      this.mostrarFormulario = false;
      this.item = null;
    }
  }

}
