import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoFirebaseComponent } from './listado-firebase.component';

describe('ListadoFirebaseComponent', () => {
  let component: ListadoFirebaseComponent;
  let fixture: ComponentFixture<ListadoFirebaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoFirebaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoFirebaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
